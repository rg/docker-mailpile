# Mailpile Docker image

A Mailpile Docker image based on a debian slim version, using official Mailpile repository and packages.

## Usage

```
docker run -d --name mailpile -p 33411:33411 rgarrigue/mailpile
```

Then open http://localhost:33411 

Bind volume `/root/.local/share/Mailpile` and `/root/.gnupg` for persistent storage.

For more information and usages, `docker run rgarrigue/mailpile help`

## Anything else

Go for https://mailpile.is, or https://github.com/mailpile/Mailpile
