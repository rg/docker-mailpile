FROM debian:buster-slim

RUN apt-get update && \
    apt-get install -y curl apt-transport-https gnupg && \
    curl -s https://packages.mailpile.is/deb/key.asc | apt-key add - && \
    echo "deb https://packages.mailpile.is/deb release main" | tee /etc/apt/sources.list.d/000-mailpile.list && \
    apt-get update && \
    apt-get install -y mailpile && \
    # TODO Enable tor
    # update-rc.d tor defaults && \
    # service tor start && \
    mailpile setup
    # TODO Enable apache for multi users
    # apt-get install -y mailpile-apache2

EXPOSE 33411
ENTRYPOINT ["mailpile"]
CMD ["--www=0.0.0.0:33411/","--wait"]

VOLUME /root/.local/share/Mailpile
VOLUME /root/.gnupg
